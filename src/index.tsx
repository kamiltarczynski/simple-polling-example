import React from "react";
import { render } from "react-dom";

import StatisticsCharts from "components/pages/StatisticsCharts";

const AppNode = document.getElementById("app");

render(<StatisticsCharts />, AppNode);
