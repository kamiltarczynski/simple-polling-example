import React from "react";

// alphaetical imports: first libraries, then our imports(helpers, components etc), then styling
import BarChart from "../../molecules/BarChart";
import LineChart from "../../molecules/LineChart";

import { ChartColumn, ChartsRow } from "./styles";

// example typesetting for props and state
interface Props {
  readonly someFn: () => void;
}

interface State {
  readonly someStateValue: string | null;
}

export class Charts extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      someStateValue: null
    };
  }

  render() {
    return (
      <React.Fragment>
        <ChartsRow>
          <ChartColumn>
            <LineChart />
          </ChartColumn>
          <ChartColumn>
            <BarChart />
          </ChartColumn>
        </ChartsRow>
      </React.Fragment>
    );
  }
}

export default Charts;
