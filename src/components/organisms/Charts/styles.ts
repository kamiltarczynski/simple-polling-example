import styled from "styled-components";

export const ChartsRow = styled.div`
  align-items: center;
  display: flex;
  font-size: 18px;
  height: 100%;
  flex: 1;
  justify-content: center;
`;

export const ChartColumn = styled.div`
  display: flex;
  padding: 40px;
  width: 50%;
`;
