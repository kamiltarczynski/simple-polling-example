import React from "react";

import axios from "axios";

import { apiUrls } from "../../../utils/apiUrls";
import { Poller } from "../../../services/poller";

import { Page } from "./styles";

class BarChart extends React.PureComponent {
  state = {
    contacts: [],
    counter: 0
  };

  updateChartData = (): void => {
    axios
      .get(apiUrls.barChart)
      .then(res => {
        console.log("line chart", res);
        this.setState({ contacts: res.data, counter: this.state.counter + 1 });
      })
      .catch(error => {
        console.log("line chart", error);
      });
  };

  componentDidMount() {
    Poller.start(
      20000, //in ms
      this.updateChartData
    );
  }

  componentWillUnmount() {
    Poller.stop();
  }

  render() {
    return (
      <Page>
        {this.state.contacts.length > 0 ? (
          <div>
            We have the bar chart data. Times updated: {this.state.counter}
          </div>
        ) : (
          <div>Bar chart data loading</div>
        )}
      </Page>
    );
  }
}

export default BarChart;
