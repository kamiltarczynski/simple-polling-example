import styled from "styled-components";

export const Page = styled.div`
  align-items: center;
  display: flex;
  font-size: 18px;
  height: 100%;
  flex: 1;
  justify-content: center;
`;
