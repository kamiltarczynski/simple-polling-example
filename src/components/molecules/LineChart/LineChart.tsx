import React from "react";

import axios from "axios";

import { Poller } from "../../../services/poller";
import { apiUrls } from "../../../utils/apiUrls";

import { Page } from "./styles";

class LineChart extends React.PureComponent {
  state = {
    contacts: [],
    counter: 0
  };

  updateChartData = (): void => {
    axios
      .get(apiUrls.lineChart)
      .then(res => {
        console.log("line chart", res);
        this.setState({ contacts: res.data, counter: this.state.counter + 1 });
      })
      .catch(error => {
        console.log("line chart", error);
      });
  };

  componentDidMount() {
    Poller.start(
      10000, //in ms
      this.updateChartData
    );
  }

  componentWillUnmount() {
    Poller.stop();
  }

  render() {
    return (
      <Page>
        {this.state.contacts.length > 0 ? (
          <div>
            We have the line chart data. Times updated: {this.state.counter}
          </div>
        ) : (
          <div>Line chart data loading</div>
        )}
      </Page>
    );
  }
}

export default LineChart;
