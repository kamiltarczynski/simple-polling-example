import React from "react";

import Charts from "../../organisms/Charts";

import {} from "./styles";

class StatisticsCharts extends React.PureComponent {
  someFn = () => {
    // do something
  };

  render() {
    // Remove the whole someFn={someFn} prop and see what happens, this is what happens if I don't pass props
    // declared in a component typing. Thanks to this we will never forget to pass a prop
    // and we will never pass a bad type of prop

    return (
      <React.Fragment>
        <Charts someFn={this.someFn} />
      </React.Fragment>
    );
  }
}

export default StatisticsCharts;
