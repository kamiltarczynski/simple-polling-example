// short polling
export const Poller = {
  // typing expected arguments, we won't be able to pass less or more arguments or argument of bad type
  start: (timeout: number, fn: () => void): void => {
    fn();
    window.setInterval(() => fn(), timeout);
  },
  stop: (): void => {
    // the logic responsible for "killing" the poller after component unmount goes here
  }
};

// TODO: long polling
